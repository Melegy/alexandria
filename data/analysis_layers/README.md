## Introduction
This directory contains the output files from the Boarding/Alighting and Passenger Flow Analysis code found [here](https://gitlab.com/transportforcairo/software/TfC_Tools/-/tree/master/Analysis/Boarding_Alighting). The analysis was run on the data from the DT4A Alex data collection project.

## Content Description

```
tree .
```


| File name                                       | Description                                                                                                                                                                                                          |
| ----------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| daily_board_alight_agency_interval_wide.geojson | Boarding/Alighting and Vehicle Flow figures for processed stops (i.e GTFS stops). Figures for stops with identical names are summed in this layer into one data point. So the stops in this layer represent "areas". |
| passenger_flow_debugging.geojson                | Pass. flow figures for every trip segment. Very detailed.                                                                                                                                                            |
| passenger_flow_mpp_net_2.geojson                | Pass. flow figures superimposed on simplified transit network created using `stplanr::overline2` in the analysis code.                                                                                               |
| passenger_flow_mpp_net.geojson                  | Pass. flow figures superimposed on simplified transit network created by method 2 (using `sfnetworks` package) in the analysis code.                                                                                 |
| passenger_flow_mpp_Bus.geojson                  | Pass. flow for Bus network                                                                                                                                                                                           |
| passenger_flow_mpp_Minibus.geojson              | Pass. flow for Minibus network                                                                                                                                                                                       |
| passenger_flow_mpp_P_B_8.geojson                | Pass. flow for Microbus network                                                                                                                                                                                      |
| passenger_flow_mpp_P_O_14.geojson               | Pass. flow for Tomnaya network                                                                                                                                                                                       |